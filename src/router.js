import Vue from 'vue'
import VueRouter from 'vue-router'

import Faculty from './components/Faculty'
import Branch from './components/Branch'
import DownloadForm from './components/DownloadForm'
import PositionCategory from './components/PositionCategory'

Vue.use(VueRouter)

const router = new VueRouter({
  mode: 'history',
  routes: [
    { path: '/Faculty', component: Faculty },
    { path: '/Branch', component: Branch },
    { path: '/DownloadForm', component: DownloadForm },
    { path: '/PositionCategory', component: PositionCategory },
    { path: '*', redirect: '/Faculty' }
  ]
})

export default router

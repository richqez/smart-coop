// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import firebase from 'firebase'
import VueStash from 'vue-stash'
import store from './store'
import VueMaterial from 'vue-material'
import VuePaginate from 'vue-paginate'

import '!script!jquery'
import '!script!toastr/build/toastr.min.js'

import '!style!css!vue-material/dist/vue-material.css'
import '!style!css!./assets/global/plugins/font-awesome/css/font-awesome.min.css'
import '!style!css!./assets/global/plugins/simple-line-icons/simple-line-icons.min.css'
import '!style!css!./assets/global/plugins/bootstrap/css/bootstrap.min.css'
import '!style!css!./assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css'
import '!style!css!./assets/global/plugins/bootstrap-daterangepicker/daterangepicker.min.css'
import '!style!css!./assets/global/plugins/fullcalendar/fullcalendar.min.css'
import '!style!css!./assets/global/plugins/jqvmap/jqvmap/jqvmap.css'
import '!style!css!./assets/global/plugins/datatables/datatables.min.css'
import '!style!css!./assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css'
import '!style!css!./assets/global/plugins/select2/css/select2.min.css'
import '!style!css!./assets/global/plugins/select2/css/select2-bootstrap.min.css'
import '!style!css!./assets/global/css/components-md.min.css'
import '!style!css!./assets/global/css/plugins-md.min.css'
import '!style!css!./assets/layouts/layout3/css/layout.min.css'
import '!style!css!./assets/layouts/layout3/css/themes/default.min.css'
import '!style!css!./assets/layouts/layout3/css/custom.min.css'
import '!style!css!toastr/build/toastr.min.css'

// import '!script!./assets/global/plugins/jquery.min.js'
import '!script!./assets/global/plugins/bootstrap/js/bootstrap.min.js'
import '!script!./assets/global/plugins/js.cookie.min.js'
import '!script!./assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js'
import '!script!./assets/global/plugins/jquery.blockui.min.js'
import '!script!./assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js'
import '!script!./assets/global/plugins/moment.min.js'
import '!script!./assets/global/plugins/bootstrap-daterangepicker/daterangepicker.min.js'
import '!script!./assets/global/plugins/counterup/jquery.waypoints.min.js'
import '!script!./assets/global/plugins/counterup/jquery.counterup.min.js'
import '!script!./assets/global/plugins/fullcalendar/fullcalendar.min.js'
import '!script!./assets/global/plugins/flot/jquery.flot.min.js'
import '!script!./assets/global/plugins/flot/jquery.flot.resize.min.js'
import '!script!./assets/global/plugins/flot/jquery.flot.categories.min.js'
import '!script!./assets/global/plugins/jquery.sparkline.min.js'
import '!script!./assets/global/scripts/datatable.js'
import '!script!./assets/global/plugins/datatables/datatables.min.js'
import '!script!./assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js'
import '!script!./assets/global/scripts/app.min.js'
import '!script!./assets/pages/scripts/table-datatables-managed.min.js'
import '!script!./assets/layouts/layout3/scripts/layout.min.js'
import '!script!./assets/layouts/layout3/scripts/demo.min.js'
import '!script!./assets/layouts/global/scripts/quick-sidebar.min.js'

// Initialize Firebase
var config = {
  apiKey: 'AIzaSyCJX-yqXrPn9a2zKED8dBlP6KOQ8NqYbzw',
  authDomain: 'smart-coop-9388d.firebaseapp.com',
  databaseURL: 'https://smart-coop-9388d.firebaseio.com',
  storageBucket: 'smart-coop-9388d.appspot.com',
  messagingSenderId: '676930052673'
}
firebase.initializeApp(config)

Vue.use(VueStash)
Vue.use(VueMaterial)
Vue.use(VuePaginate)

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  data: { store },
  render (h) {
    return h(App)
  }
})

import firebase from 'firebase'
import Auth from './auth'

const getAll = (cb) => {
  firebase.database().ref('faculty')
  .on('value', (snapshots) => {
    let rs = []
    snapshots.forEach(v => {
      rs.push({
        key: v.key,
        value: v.val()
      })
    })
    rs.reverse()
    rs = rs.map(fa => {
      return {
        ...fa.value,
        key: fa.key
      }
    })
    cb(rs)
  })
}

const add = (facultyCode, facultyName, facultyDesc) => {
  return firebase.database().ref('faculty')
  .push({
    facultyCode,
    facultyName,
    facultyDesc,
    timestamp: firebase.database.ServerValue.TIMESTAMP,
    createdBy: Auth.getCurrentUser().uid
  })
}

const isExist = (code, callback) => {
  firebase.database().ref('faculty')
  .orderByChild('facultyCode')
  .equalTo(code)
  .once('value', snapShot => {
    callback(snapShot.exists())
  })
}

const update = (faculty) => {
  const key = faculty.key
  delete faculty.key
  return firebase.database()
    .ref(`faculty/${key}`)
    .set(faculty)
}

const remove = (faculty) => {
  return firebase.database()
    .ref(`faculty`)
    .child(faculty.key)
    .remove()
}

export default {
  getAll,
  isExist,
  add,
  update,
  remove
}

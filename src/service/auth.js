import firebase from 'firebase'

const getCurrentUser = () => firebase.auth().currentUser

const login = (email, passwd, success, error) => {
  firebase.auth().signInWithEmailAndPassword(email, passwd)
  .then(success)
  .catch(error)
}

const logout = (callback) => {
  firebase.auth().signOut().then(callback)
}

const resetPassword = (email, success, error) => {
  firebase.auth().sendPasswordResetEmail(email)
  .then(success)
  .catch(error)
}

export default {
  getCurrentUser,
  login,
  logout,
  resetPassword
}

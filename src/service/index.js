
import Auth from './auth'
import Faculty from './faculty'
import Branch from './branch'
export {
    Auth,
    Faculty,
    Branch
}
